package HeroAndAHorse;

public class Hero implements ICharacter {

    private String name;

    private int health = 100, attackPoints = 50, manaPoints = 30, defencePoints = 10;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return 0;
    }

    public int getHealth() {
        return health;
    }

    public int getAttackPoints() {
        return attackPoints;
    }

    public int getDefencePoints() {
        return defencePoints;
    }

    public void setHelth(int helth) {
        this.health = helth;
    }


    public void setAttackPoints(int attackPoints) {
        this.attackPoints = attackPoints;
    }

    public int getManaPoints() {
        return manaPoints;
    }

    public void setManaPoints(int manaPoints) {
        this.manaPoints = manaPoints;
    }

    public void setDefencePoints(int defencePoints) {
        this.defencePoints = defencePoints;
    }

    @Override
    public String toString() {
        return "HeroAndAHorse.Hero{" +
                "name='" + name + '\'' +
                ", helth=" + health +
                ", attackPoints=" + attackPoints +
                ", manaPoints=" + manaPoints +
                ", defencePoints=" + defencePoints +
                '}';
    }
}
