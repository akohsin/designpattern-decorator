package HeroAndAHorse;

public interface ICharacter {
    int getHealth();
    int getSpeed();
    int getAttackPoints();
    int getDefencePoints();
}
