package HeroAndAHorse;

public class HeroOnHorse implements ICharacter {
    private ICharacter hero;

    public HeroOnHorse(ICharacter hero) {
        this.hero = hero;
    }

    public int getHealth() {
        return hero.getHealth() + 100;
    }

    public int getSpeed() {
        return hero.getSpeed() * 2;
    }

    public int getAttackPoints() {
        return hero.getAttackPoints() + 30;
    }

    public int getDefencePoints() {
        return hero.getDefencePoints() + 30;
    }

    @Override
    public String toString() {
        return "HeroAndAHorse.HeroOnHorse{" +
                ", helth=" + getHealth() +
                ", attackPoints=" + getAttackPoints() +
                ", defencePoints=" + getDefencePoints() +
                ", speed="+getSpeed()+
                '}';
    }
}
