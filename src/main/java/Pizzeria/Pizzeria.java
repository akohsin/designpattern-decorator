package Pizzeria;

import java.util.ArrayList;
import java.util.List;

public class Pizzeria {
    List<SimplePizza> menu = new ArrayList<SimplePizza>();

    public Pizzeria() {
        }


    public SimplePizza withDoubleCheese(SimplePizza pizza){
        return new CustomPizza(pizza, true, false, 2.50);
    }
    public SimplePizza withOlives(SimplePizza pizza){
        return new CustomPizza(pizza, false, true, 5.0);
    }

    public static void main(String[] args) {
        Pizzeria pizzeria = new Pizzeria();
        System.out.println(pizzeria.withOlives(new MargeritaPizza()).toString());
    }

}
