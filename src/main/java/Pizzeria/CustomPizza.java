package Pizzeria;

import java.util.List;

public class CustomPizza implements IDecoratedPizza {
    SimplePizza pizza;
    boolean doubleCheese = false;
    boolean olives = false;
    double price;
    List<String> lista;
    public CustomPizza(SimplePizza custom, boolean doubleCheese, boolean olives, double doplata) {
        this.pizza = custom;
        this.doubleCheese = doubleCheese;
        this.olives = olives;
        price = pizza.getPrice();
        lista = pizza.getIngredients();

        if (this.doubleCheese) {
            lista.add("podwojny ser");
            price += 5;
        }
        if (this.olives) {
            lista.add("oliwki");
            price += 5;
        }
    }

    public List<String> getIngredients() {
        return lista;
    }

    public double getPrice() {
        return price;
    }
}
