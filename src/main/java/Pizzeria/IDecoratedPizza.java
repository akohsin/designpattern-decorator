package Pizzeria;

import java.util.List;

public interface IDecoratedPizza {
    List<String> getIngredients();
    double getPrice();
}
