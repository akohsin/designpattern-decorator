package Pizzeria;

import java.util.ArrayList;
import java.util.List;

public class SimplePizza implements IDecoratedPizza {
    double price;
    List<String> ingredients;
    List<String> extras = new ArrayList<String>();

    private SimplePizza() {
    }

    public void addExtras(String... extras) {
        for (String x : extras) {
            this.extras.add(x);
        }
    }


    public double getPrice() {
        return price;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public SimplePizza(double price, List<String> ingredients, String... extras) {
        this.price = price;
        this.ingredients = ingredients;
        for (String x : extras) {
            this.extras.add(x);
        }
    }

    @Override
    public String toString() {
        return "SimplePizza{" +
                "price=" + price +
                ", ingredients=" + ingredients +
                ", extras="+extras +
                '}';
    }
}
