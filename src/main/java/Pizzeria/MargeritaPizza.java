package Pizzeria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MargeritaPizza implements IDecoratedPizza {
    SimplePizza pizza;

    public MargeritaPizza(SimplePizza pizza) {
        this.pizza = pizza;
    }

    public double getPrice() {
        return pizza.getPrice() + 4;
    }


    public List<String> getIngredients() {
        List<String> lista = new ArrayList<String>();
        lista = pizza.getIngredients();
        lista.add("sos");
        lista.add("ser");
        return pizza.getIngredients();
    }
}
