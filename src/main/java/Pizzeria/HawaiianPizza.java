package Pizzeria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HawaiianPizza implements IDecoratedPizza {
    SimplePizza pizza;
    public HawaiianPizza(SimplePizza pizza) {
        this.pizza = pizza;
    }


    public double getPrice() {

        return pizza.getPrice() + 6;
    }


    public List<String> getIngredients() {
        List<String> lista = pizza.getIngredients();
        lista.add("ser");
        lista.add("ananaas");
        return lista;
    }
}
