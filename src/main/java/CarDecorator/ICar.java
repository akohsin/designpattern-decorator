package CarDecorator;

public interface ICar {

    int getHorsePower();

    boolean hasCharger();

    double getEngineCapacity();

    int getChargerPressure();
}



