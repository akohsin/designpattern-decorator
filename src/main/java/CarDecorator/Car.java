package CarDecorator;

public class Car implements ICar{
    private int chargerPressure;
    private double engineCapacity;
    private int seatsNumber;
    private boolean hasCharger;
    private int horsePower;

    public int getHorsePower() {
        return horsePower;
    }

    public boolean hasCharger() {
        return hasCharger;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public int getChargerPressure() {
        return chargerPressure;
    }

    public void setChargerPressure(int chargerPressure) {
        this.chargerPressure = chargerPressure;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public void setHasCharger(boolean hasCharger) {
        this.hasCharger = hasCharger;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }
}
