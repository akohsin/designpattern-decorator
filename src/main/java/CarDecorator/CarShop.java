package CarDecorator;

public class CarShop {

    public ICar addExtraCharger(ICar c) {
        return new TunedCard(c, true, false);
    }

    public ICar addExtraEngine(ICar c) {
        return new TunedCard(c, false, true);
    }
}
