package CarDecorator;

public class TunedCard implements ICar {
    private ICar car;
    private boolean extraCharger = false;
    private boolean extraEngine = false;

    public TunedCard(ICar car, boolean extraCharger, boolean extraEngine) {
        this.car = car;
        if (!car.hasCharger()) {
            this.extraCharger = extraCharger;
        }
        else{
            this.extraEngine = extraEngine;
        }
    }

    public int getHorsePower() {
        return car.getHorsePower() * (extraCharger ? 2 : 1) * (extraEngine ? 2 : 1);
    }

    public boolean hasCharger() {
        return extraCharger;
    }

    public double getEngineCapacity() {
        return extraEngine ? car.getEngineCapacity() * 2 : car.getEngineCapacity();
    }

    public int getChargerPressure() {
        return car.getChargerPressure() * (extraCharger ? 2 : 1);
    }
}
